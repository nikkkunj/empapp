var fs = require('fs');
var express = require('express');
var cors = require('cors');
var app =express();
var bodyParser = require('body-parser');
var Datastore = require('nedb');  
var employees = new Datastore({ filename: 'employees.db', autoload: true }); 

app.listen(9000,function(){
	console.log("Server running @ port:9000");
});
app.use(bodyParser.json());
app.use(cors());

//List Employees
app.get('/listEmployees',function(request,response,next){
   employees.find({}, function(err, docs) {  
		response.end(JSON.stringify(docs));
	});
});

//Add Employee
app.post('/addEmployee', function (req, res) {
	console.log(req.body);
	
	employees.insert(req.body, function(err, doc) {  
		console.log('Inserted', doc.fname, 'with ID', doc._id);
		 res.end( JSON.stringify(req.body));
	});
 
});

//Delete Employee
app.post('/deleteEmployee', function (req, res) {
	console.log(req.body);
	employees.remove({ _id: req.body.id }, function(err, numDeleted) {  
		console.log('Deleted', numDeleted, 'user(s)');
		 res.end(JSON.stringify({"error":"0","msg":"success"}));
	});
});

//Update Employee
app.post('/updateEmployee', function (req, res) {
	console.log(req.body);
	var ed_data =req.body;
	employees.update({ _id: ed_data.id }, { $set: { fname:ed_data.fname,lname:ed_data.lname,age:ed_data.age,email:ed_data.email,gender:ed_data.gender} }, { multi: true }, function (err, numReplaced) {
	 res.end(JSON.stringify({"error":"0","msg":"success"}));
	});

});
