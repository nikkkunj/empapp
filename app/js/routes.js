'use strict';
employeeApp.config(['$stateProvider','$urlRouterProvider','$httpProvider', function ($stateProvider,$urlRouterProvider,$httpProvider) {
	//$httpProvider.interceptors.push('myHttpInterceptor');
	$urlRouterProvider.otherwise('/home');
	$stateProvider.
	state('home', {
		url:'/home',
		 templateUrl: '../templates/home.html',
		 controller: 'mainController'
	}).
	state('addemp', {
		url:'/add-new',
		templateUrl: '../templates/empreg.html',
		controller: 'mainController'
	}).
	state('details', {
		url:'/emplist',
		templateUrl: '../templates/emplist.html',
		controller: 'empListController'
	});
}]);
