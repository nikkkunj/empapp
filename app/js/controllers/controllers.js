   'use strict';
    employeeApp.controller('mainController', ['$scope','APIservice',function ($scope,APIservice) {
		$scope.emp ={};
		$scope.emp.gender ="male";
		$scope.AddEmp = function(){
			APIservice.addEmployee($scope.emp).then(function(response){			
					delete $scope.emp;
					$scope.emp ={};
					$scope.emp.gender ="male";
					alert('Employee Addedd successfully..!!');

				},function(error){
					alert('Error in updating data!!');
					
				});
		 }	
	}]);
	
	employeeApp.controller('empListController', ['$scope','APIservice','$state',function ($scope,APIservice,$state) {
		$scope.empdata ={};
		APIservice.listEmployee($scope.emp).then(function(response){	
	//		console.log(response.data);		
			$scope.empdata  = response.data;
			},function(error){
				alert('Error in API!!');
		});
			
		//Delete Employee
		$scope.delEmp = function (emp_id){
			$scope.delemp = {};
			$scope.delemp.id=emp_id;
			APIservice.deleteEmployee($scope.delemp).then(function(response){

			alert('Employee Deleted successfully..!!');
			$state.reload();
			},function(error){						
				alert('Error in Deleting data!!');
			});
		};
		
		//Edit Employee
		$scope.editEmpMod = function (emp){
			$scope.editemp = {};
			$scope.editemp.fname = emp.fname;
			$scope.editemp.lname = emp.lname;
			$scope.editemp.age = emp.age;
			$scope.editemp.email = emp.email;
			$scope.editemp.gender = emp.gender;
			$scope.editemp.id = emp._id;
		};
		
		$scope.editEmployee = function (){
			console.log($scope.editemp);
			APIservice.updateEmployee($scope.editemp).then(function(response){
				$state.reload();
				alert('Employee Updated successfully..!!');
			},function(error){						
				alert('Error in Updating data!!');
			});
		};
	}]);
