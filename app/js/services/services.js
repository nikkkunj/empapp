	'use strict';
    employeeApp.factory('APIservice',['$http',function($http){
		var APIurl = 'http://localhost:9000';
		var APIservice = {};
		
		APIservice.addEmployee =function(data){
			return $http({
				method:'POST',
				url:APIurl+'/addEmployee',				
				data:JSON.stringify(data),
				contentType: "application/json",
				dataType: "json"
			});	
		};
		
		APIservice.updateEmployee =function(data){
			
			return $http({
				method:'POST',
				url:APIurl+'/updateEmployee',				
				data:data				
			});
		};
		
		APIservice.listEmployee =function(data){
			return $http({
				method:'GET',
				url:APIurl+'/listEmployees'			
			});
		};
		
		APIservice.deleteEmployee =function(data){
			return $http({
				method:'POST',
				url:APIurl+'/deleteEmployee',
				data:data,
				contentType: "application/json",
				dataType: "json"
			});
		};
		
		return APIservice;
	}]);
