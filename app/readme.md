Excluded node modules to reduce the folder size for sending on mail

To Run APP follow below procedures
Install nodejs & NPM if not there
1> Go to webserservices folder and run follwing command
	npm install express --save
	npm install body-parser --save
	change the file permission of empdata.json to 777
2>run node server.js

3>open browser and type " http://localhost/employ "
if localhost not working in your system and you are running through IP change 'localhost' to 'IP' in js/services/services.js file 

Note: I have used node and saving data in json file instead of angular httpBackend or local storage.
enabled CORS as node is not my default webserver
